CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Customization
* Maintainers

INTRODUCTION
------------

This module provides integration with BaseLinker
integration platform (baselinker.com).

REQUIREMENTS
------------

This module requires Drupal 'commerce' module in order to integrate
BaseLinker with your Drupal commerce site.
You need to download/install it first in order to use this module.

INSTALLATION
------------

Install normally as other modules are installed. For Support:
https://www.drupal.org/docs/8/extending-drupal/installing-contributed-modules

CONFIGURATION
-------------

Log in to your baselinker.com panel and go to Integrations / Add integration.
Type "Drupal commerce" into the search bar or find it under the "Shop" category.
Fill in the "Data exchange file" field with the pattern
{{ your site URL }}/baselinker ex.: https://example.com/baselinker
Copy communication password value.

Open your Drupal commerce site and go to the BaseLinker configuration page
/admin/commerce/baselinker/config. Paste copied password from BaseLinker panel
into "Communication password" field.

If you need to adjust the integration file version and standard,
please do it at your own risk.
We do not recommend to change this value in any case without
a particular reason.

Submit configuration form.

You need to have these modules enabled:
- Commerce Payment
- Commerce Shipment

A payment gateway and a shipment method must be created in order to correctly send data to baselinker.
This can be done in the Drupal Commerce configuration.

Additionally, a product variation type must have a "shippable" trait selected
and a product weight assigned to it. This can also be done in the Drupal Commerce configuration.

CUSTOMIZATION
-------------
In order to send custom email or phone number to the BaseLinker,
you will have to go to: /admin/config/people/profile-types
And click "Manage fields" in your selected profile (Customer by default) ex:
/admin/config/people/profile-types/manage/customer/fields

Add new fields: field_email and field_phone.
By default if the field_email does not exist, user email or guest email
will be provided.

MAINTAINERS:
----------------------
Maintainers:
Tomasz Makiel https://www.drupal.org/u/tmakiel
Mariusz Andrzejewski https://drupal.org/u/sayco
Piotr Kamieniecki https://www.drupal.org/u/piotrkamieniecki
